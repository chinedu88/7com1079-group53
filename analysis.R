library(tidyverse)

Dataset3 <- read_delim("Dataset3.csv", ";", escape_double = FALSE, trim_ws = TRUE)
pdf("visualization.pdf")
df <- as.data.frame(Dataset3)

names(Dataset3)[7]<-"Global.Entrepreneurship.Index"
names(df)[7]<-"Global Entrepreneurship Index"
df$`Women Entrepreneurship Index`<-as.numeric(df$`Women Entrepreneurship Index`)
df$`Global Entrepreneurship Index`<-as.numeric(df$`Global Entrepreneurship Index`)
x <- head(df$`Women Entrepreneurship Index`, 20)
y <- head(df$`Global Entrepreneurship Index`, 20)

cor.test(x,y,use="pairwise.complete.obs")

plot(x, y, main = "Women vs Global Entrepreneurship Index", xlab = "Women Entrepreneurship", ylab = "Global Entrepreneurship", las=1, 
     xlim = c(40,70), ylim = c(35,80), col.main= "magenta", col = "purple", col.axis = 'blue', col.lab = 'red', pch=19)
abline(model, col = "red")
model <- lm(y ~ x, data = df)
hist(y, xlab = "Women Enterpreneurship Index", ylab = "Global Entrepreneurship Index")
hist(x, xlab = "Women Enterpreneurship Index", ylab = "Global Entrepreneurship Index")
dev.off()

